﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogLibrary
{
    public  class ProtocolEnum
    {
       

    }
    public static class Constants
    {
        public static string P2BasicTracker = "P2BasicTracker";
        public static string AssetTracker = "AssetTracker";
        public static string BikeTracker = "BikeTracker";
        public static string P2SerialDevice = "P2SerialDevice";
        public static string OBDDevice = "OBDDevice";
        public static string L1Multifunctional = "L1Multifunctional";
    }
    //Transync Protocol splited into bytes
    public enum Transync
    {
        //Allocated on bytes
        StartByte = 2,
        PacketLegnth = 1,
        LAC = 2,
        DeviceID = 8,
        InfoSerialNo = 2,
        ProtocolNumber = 1,
        DateTime = 6,
        Latitude = 4,
        Longitude = 4,
        Speed = 1,
        Course = 2,
        MNC = 1,
        CellID = 2,
        StatusByte = 4,
        GSMSignalStregnth = 1,
        VoltageLevel = 1,
        NoOfSatellite = 1,
        HDOP = 1,
        ADCValue = 2,
        OdometerIndex = 1,
        OdometerLegnth = 1,
        OdometerReading = 5,
        RFIDIndex = 1,
        RFIDLegnth = 1,
        RFIDTag = 5,
        StopByte = 2

    }
    public enum Status_BYTE0
    {
        Bit0 = 0,//0-Not fixed,1-Fixed
        Bit1 = 1,//0-South,1-North
        Bit2 = 2,//0-West,1-East,
        Bit3 = 3,//0-Off,1-On,
        Bit4 = 4,//0-On,1-off,
        Bit5 = 5,//0-Off,1-On,
        Bit6 = 6,//0-Off,1-On,
        Bit7 = 7 //0-Off,1-On

    }
    public enum Status_BYTE1
    {
        Bit8 = 8,//Reserved
        Bit9 = 9,//Reserved
        Bit10 = 10,//Reserved
        Bit11 = 11,//Reserved
        Bit12 = 12,//Reserved
        Bit13 = 13,//Reserved
        Bit14 = 14,//Reserved
        Bit15 = 15 //Reserved

    }
    public enum Status_BYTE2
    {
        Bit16= 16,//reserved
        Bit17 = 17,//reserved
        Bit18 = 18,//reserved
        Bit19 = 19,//reserved
        Bit20 = 20,//1-Immobilization Alert
        Bit21 = 21,//1-Low Battery Alert
        Bit22 = 22,//1-SOS Alert
        Bit23 = 23//1-Overspeed alert

    }
    public enum Status_BYTE3
    {
        Bit24 = 24, Bit25 = 25, Bit26 = 26, Bit27 = 27,//bit 0,1,2,3 represents the type of device ex 0010-Assert tracker
        Bit28 = 28,//Reserved
        Bit29 = 29,//GpsNot responding Alert
        Bit30 = 30,//1-HighResolution data,
        Bit31 = 31//0-Livedata,1-saved data

    }
}
