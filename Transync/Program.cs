﻿using LogLibrary;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Transync
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                String Banner=@"
        ████████╗██╗  ██╗██╗███╗   ██╗████████╗██╗   ██╗██████╗ ███████╗
        ╚══██╔══╝██║  ██║██║████╗  ██║╚══██╔══╝██║   ██║██╔══██╗██╔════╝
           ██║   ███████║██║██╔██╗ ██║   ██║   ██║   ██║██████╔╝█████╗  
           ██║   ██╔══██║██║██║╚██╗██║   ██║   ██║   ██║██╔══██╗██╔══╝  
           ██║   ██║  ██║██║██║ ╚████║   ██║   ╚██████╔╝██║  ██║███████╗
           ╚═╝   ╚═╝  ╚═╝╚═╝╚═╝  ╚═══╝   ╚═╝    ╚═════╝ ╚═╝  ╚═╝╚══════╝
                                     (V1.0.0)                                                            
";
                Console.WriteLine("\t"+Banner);
                ServerManager objserver = new ServerManager();
                objserver.Server();
            }
            
            catch(SocketException ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

       
    }
}
