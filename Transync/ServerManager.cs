﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using LogLibrary;
namespace Transync
{
    public class ServerManager
    {
        private TcpListener Listener;
        private Thread ClientThread;
        System.Configuration.AppSettingsReader settingsReader = new AppSettingsReader();
       // private Logs Logs;
        private int _bytesize, _port;
        string LogDirectory, ExceptionDirectory;
        //Start the Thread 
        public void Server()
        {
            _bytesize = (int)settingsReader.GetValue("BYTESIZE", typeof(int));
          
            _port=(int)settingsReader.GetValue("PORT", typeof(int));
            LogDirectory = (string)settingsReader.GetValue("LOGDIRECTORY", typeof(string));
            ExceptionDirectory = (string)settingsReader.GetValue("EXEDIRECTORY", typeof(string));
           // Logs = new Logs();
            Listener = new TcpListener(IPAddress.Any,_port);
            ClientThread = new Thread(new ThreadStart(ClientListner));
            ClientThread.Start();
            
            
        }
        //ClientThread
        private void ClientListner()
        {
            try
            {
                this.Listener.Start();
                Console.WriteLine("Service port opened({0}) !!", _port);
                Logs.DataLog(DateTime.Now.ToString() + "\t" + "Service Stared",LogDirectory);
                while (true)
                {
                    TcpClient client = this.Listener.AcceptTcpClient();
                    Thread clientThread = new Thread(() => HandleReceiver(client));
                    clientThread.Start();
                }
            }
            catch (Exception ex)
            {
                Logs.ExcetpionLog(DateTime.Now.ToString() + "\t" + ex.ToString()+"\n",ExceptionDirectory);
            }

        }
        //Handle income Request 
        private void HandleReceiver(TcpClient client)
        {
            try
            {
                TcpClient tcpClient = (TcpClient)client;
                int bytesRead;
                byte[] message = new byte[_bytesize];
                TransyncFormatter Formatter = new TransyncFormatter();
                string RawString=string.Empty;
                if (tcpClient.Connected)
                {
                    NetworkStream clientStream = tcpClient.GetStream();
                    while (true)
                    {
                        bytesRead = 0;
                        try
                        {
                            //blocks until a client sends a message
                            Array.Clear(message, 0, message.Length);
                            Array.Resize(ref message, _bytesize);
                            bytesRead = clientStream.Read(message, 0, _bytesize);

                        }
                        catch (Exception ex)
                        {
                            //Console.WriteLine("socket error occured...." + ex.Message);
                            Logs.ExcetpionLog(DateTime.Now.ToString() + "\t" + ex.ToString() + "\n",ExceptionDirectory);
                            clientStream.Close();
                            tcpClient.Close();
                            try
                            {
                                Thread.CurrentThread.Abort();
                            }
                            catch (ThreadAbortException)
                            {
                                Logs.ExcetpionLog(DateTime.Now.ToString() + "\t" + ex.ToString() + "\n", ExceptionDirectory);
                            }
                        }
                        if (bytesRead == 0)
                        {
                            clientStream.Flush();
                            clientStream.Close();
                            break;
                        }
                        else
                        {
                            ASCIIEncoding encoder = new ASCIIEncoding();
                           // RawString =Connection.ASCIIToHex(message);
                            RawString = BitConverter.ToString(message).Replace("-", string.Empty); 
                            Formatter.MessageSplit(RawString,LogDirectory);
                            RawString = string.Empty;
                        }
                    }
                }

                Console.WriteLine("Completed process");
            }
            catch (Exception ex)
            {
                Logs.ExcetpionLog(DateTime.Now.ToString() + "\t" + ex.ToString() + "\n",ExceptionDirectory);
            }

        }

    }
}
